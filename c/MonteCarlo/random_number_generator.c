#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

#define MIN_POINTS 100
#define MAX_POINTS 10000

void random_number_generator(float *numbers, int n_points) {
    for(int i = 0; i < n_points; ++i) {
        numbers[i] = -1 + 2 * ((float)rand()/(float)RAND_MAX);
    }
}

void main(void) {
    int n_circle, n_points;
    srand(time(NULL));
    FILE *data = fopen("data.txt", "w");

    float x[MAX_POINTS], y[MAX_POINTS];

    for(n_points = MIN_POINTS; n_points <= MAX_POINTS; n_points += 100) {
        n_circle = 0;
        random_number_generator(x, n_points);
        usleep(500);
        random_number_generator(y, n_points);
        for(int i = 0; i < n_points; ++i) {
            if(x[i] * x[i] + y[i] * y[i] < 1) {
                n_circle++;
            }
        }
        fprintf(data, "%d %f %f\n", n_points, 4.0 * ((float)n_circle) / n_points, M_PI);
        usleep(1000);
    }

    fclose(data);
}