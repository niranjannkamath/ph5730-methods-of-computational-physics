plot "data.txt" using 1:2 title "Obtained Values" w l,\
	 "data.txt" using 1:3 title "Actual Value of Pi" w l
set xlabel "Number of darts"
set ylabel "4 * (number of darts in circle) / (number of darts)"
set output "value_of_pi.ps"
set terminal postscript enhanced color
replot
quit
