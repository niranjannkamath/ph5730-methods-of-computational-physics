#include <math.h>

#ifndef FUNC
#define FUNC

double func(double x) {
    return x * x - 4 * x * sin(x) + 2 * sin(x) * sin(x);
}

#endif