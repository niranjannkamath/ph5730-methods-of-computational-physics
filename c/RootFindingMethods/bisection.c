#include <stdio.h>
#include "func.h"

double bisection(double start, double end, double tolerance) {
    double mid = start + (end - start) / 2;

    if(end - start < tolerance) {
        return mid;
    }

    if(func(start) * func(mid) < 0) {
        return bisection(start, mid, tolerance);
    }
    else if(func(mid) * func(end) < 0) {
        return bisection(mid, end, tolerance);
    }
    else {
        return mid;
    }
}

#ifndef MAIN
#define MAIN

#ifndef MAIN_1
#define MAIN_1

int main(void) {
    printf("%.6f\n", bisection(0.1, 4.0, 0.0003));

    return 0;
}

#endif
#endif