# THIS FOLDER CONTAINS SOLUTIONS WRITTEN IN C OF FIRST ASSIGNMENT ON ROOT FINDING METHODS
# IT CONTAINS THE FOLLOWING FILES

## func.h
Contains the function I use for finding the root.

## bisection.c
Contains the code for finding root using Bisection Method.

## newton_raphson.c
Contains the code for finding root using Newton-Raphson Method.

## hybrid.c
Contains the code for finding root using a hybrid of Bisection and Newton-Raphson Method.

## secant.c
Contains the code for finding root using Secant Method.

## brute_force_search.c
Contains the code for finding the intervals in which the roots of a function lie.\
\
**NOTE** In this method I have used a different function from the previous methods. This function has been defined in this same file.

## Makefile
the makefile for compiling the assignment code

**To compile any of the source code files just execute** `make <filename>` **in this directory**\
Use the filename without the `.c` extension\
\
**Alternatively you could compile the files by hand by executing** `g++ -o <filename> <filename>.c` for the file `brute_force_search.c`\
\
**PS** The bisection.c, newton_raphson.c, secant.c and hybrid.c have to be compiled using\
`gcc <filename>.c -o <filename> -lm` \
for it to detect the `sin` function in  `math.h`
