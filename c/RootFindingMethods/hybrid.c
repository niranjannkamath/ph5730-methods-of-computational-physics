#define MAIN

#include <stdio.h>
#include "bisection.c"
#include "newton_raphson.c"

double hybrid(double start, double end, double tolerance) {
    double newton_start = bisection(start, end, tolerance);
    double result = newton_raphson(newton_start, tolerance/10.0);

    return result;
}

int main(void) {
    printf("%.6f\n", hybrid(0.1, 4.0, 0.003));

    return 0;
}