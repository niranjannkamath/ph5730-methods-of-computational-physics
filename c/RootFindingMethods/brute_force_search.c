#include <stdio.h>

#define START -100.0
#define END 100.0
#define STEP  0.1

double f(double x) {
    return (x - 10.34) * (x - 13.53) * (x - 23.77);
}

int brute_force_search(double *brackets, double (*f)(double)) {
    double i = START;
    int count = 0;

    while(i <= END) {
        if((*f)(i) * (*f)(i + STEP) <= 0) {
            brackets[count] = i;
            count++;
        }

        i += STEP;
    }

    return count;
}

#ifndef MAIN_1
#define MAIN_1

int main(void) {
    double brackets[(int)((END - START) / STEP)];

    int num_brackets = brute_force_search(brackets, f);

    for(int i = 0; i < num_brackets; ++i) {
        printf("%.1f %.1f\n", brackets[i], brackets[i] + STEP);
    }

    return 0;
}

#endif