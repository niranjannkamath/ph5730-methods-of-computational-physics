plot "data.txt" using 1:2 title "Standard Deviation" w l
set xlabel "Number of darts(N)"
set ylabel "Standard Deviation"
set output "standard_deviation.ps"
set terminal postscript enhanced color
replot
quit
