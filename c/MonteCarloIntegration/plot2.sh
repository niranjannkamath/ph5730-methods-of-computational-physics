plot "data.txt" using 1:3 title "sigma * sqrt(N)" w l
set xlabel "Number of darts(N)"
set ylabel "sigma * sqrt(N)"
set output "sigma_vs_sqrt_of_N"
set terminal postscript enhanced color
replot
quit
