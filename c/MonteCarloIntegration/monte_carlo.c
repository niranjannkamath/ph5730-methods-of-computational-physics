#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define N_TRIALS 10000
#define MIN_POINTS 100
#define MAX_POINTS 10000

float func(float x) {
    return sqrt(1  - x * x);
}

void random_number_generator(float *numbers, int n_points) {
    for(int i = 0; i < n_points; ++i) {
        numbers[i] = -1 + 2 * ((float)rand()/(float)RAND_MAX);
    }
}

float monte_carlo(float *x, int n_points, float (*func)(float)) {
    float sum = 0;
    for(int i = 0; i < n_points; ++i) {
        sum += (*func)(x[i]);
    }
    return 2 * sum / n_points; 
}

float compute_standard_deviation(float *data, int size) {
    float mean = 0, squared_mean = 0;
    for(int i = 0; i < size; ++i) {
        mean += data[i];
        squared_mean += data[i] * data[i];
    }
    mean /= size;
    squared_mean /= size;

    return sqrt(squared_mean - mean  * mean);
}

void main(void) {
    FILE *output = fopen("data.txt", "w");

    srand(time(0));

    float x[MAX_POINTS], outcomes[N_TRIALS];
    for(int n_points = MIN_POINTS; n_points <= MAX_POINTS; n_points += 100) {
        for(int trial = 0; trial < N_TRIALS; ++trial) {
            random_number_generator(x, n_points);
            outcomes[trial] = monte_carlo(x, n_points, func);
        }
        float standard_deviation = compute_standard_deviation(outcomes, n_points);
        fprintf(output, "%d %.10f %.10f\n", n_points, standard_deviation, standard_deviation * sqrt(n_points));
    }

    fclose(output);
}