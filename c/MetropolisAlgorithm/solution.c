#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define MIN_POINTS 100
#define MAX_POINTS 10000

double func(double x) {
    return 1 / (1 + x * x);
}

double weight(double x) {
    return (4 - 2 * x) / 3;
}

double integrand(double y) {
    double t = sqrt(4 - 3 * y);
    return 3 / (2 * t * (9 - 3 * y - 4 * t));
}

void generate_random_points(double *x, double *y, int n_points) {
    for(int i = 0; i < n_points; ++i) {
        x[i] = (double)rand() / (double)RAND_MAX;
        y[i] = (double)rand() / (double)RAND_MAX;
    }
}

double monte_carlo(double (*f)(double), double *x, double *y, int n_points) {
    int valid_points = 0;
    for(int i = 0; i < n_points; i++) {
        if(y[i] <= (*f)(x[i])) {
            valid_points++;
        }
    }

    return ((double) valid_points) / n_points;
}

void main(void) {
    srand(time(0));
    int n_points;
    double x[MAX_POINTS], y[MAX_POINTS];
    FILE *data = fopen("data.txt", "w");
    for(n_points = MIN_POINTS; n_points <= MAX_POINTS; n_points += 100) {
        generate_random_points(x, y, n_points);
        double unweighted_result = monte_carlo((double (*)(double))func, x, y, n_points);
        double weighted_result = monte_carlo((double (*)(double))integrand, x, y, n_points);
        fprintf(data, "%d %.6f %.6f\n", n_points, unweighted_result, weighted_result);
    }
    fclose(data);
}