#include <math.h>
#include "./params.h"

double even_wavefunction(double e, double x) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    double coeff = cos(alpha * WIDTH) / exp(-beta * WIDTH);

    if(x < -WIDTH || x > WIDTH) {
        return coeff * exp(-beta * fabs(x));
    }
    else {
        return cos(alpha * x);
    }
}

double odd_wavefunction(double e, double x) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    double coeff = sin(alpha * WIDTH) / exp(-beta * WIDTH);

    if(x < -WIDTH || x > WIDTH) {
        return (fabs(x) / x) * (coeff * exp(-beta * fabs(x)));
    }
    else {
        return sin(alpha * x);
    }
}