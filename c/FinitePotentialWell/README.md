# THIS FOLDER CONTAINS SOLUTIONS WRITTEN IN C OF SECOND ASSIGNMENT ON FINITE POTENTIAL WELL
# IT CONTAINS THE FOLLOWING FILES

## params.h
Contains values of depth and width of the well

## wavefunctions.c
Contains definitions of even and odd parity wavefunctions

## solution.c
Main module for the assignment. Contains main function and other functions for finding energy eigenvalues

## wavefns_even.sh
gnuplot script for plotting data in wavefns_even.txt

## wavefns_odd.sh
gnuplot script for plotting data in wavefns_odd.txt

## Makefile
The makefile for compiling the assignment code

# The following files are generated on running make in this directory

## wavefns_even.txt
Contains data points for even parity eigenfunctions for different energy eigenvalues

## wavefns_even.ps
Plot of even parity eigenfunctions for differnt energy eigenvalues

## wavefns_odd.txt
Contains data points for odd parity eigenfunctions for different energy eigenvalues

## wavefns_odd.ps
Plot of odd parity eigenfunctions for differnt energy eigenvalues

**NOTE: The assignment can also be compiled(and the above data and plots generated) by first generating the data files and then plotting it**

**To generate the data files execute the following commands:** \
`gcc solution.cpp -o solution -lm` and then `./solution`\
\
**To generate plots execute the following commands after executing the previous commands:** \
`gnuplot wavefns_even.sh` and `gnuplot wavefns_odd.sh`\
\
**PS** The solution.c file has to be compiled using\
`gcc solution.c -o solution -lm` \
for it to detect the `sin` function in  `math.h`