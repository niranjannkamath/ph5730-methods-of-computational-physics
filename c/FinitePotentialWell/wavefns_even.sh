plot "wavefns_even.txt" using 1:2 title "E1" w l,\
     "wavefns_even.txt" using 1:3 title "E2" w l,\
     "wavefns_even.txt" using 1:4 title "E3" w l,\
     "wavefns_even.txt" using 1:5 title "E4" w l,\
     "wavefns_even.txt" using 1:6 title "E5" w l,\
     "wavefns_even.txt" using 1:7 title "E6" w l,\
     "wavefns_even.txt" using 1:8 title "E7" w l,\
     "wavefns_even.txt" using 1:9 title "E8" w l,\
     "wavefns_even.txt" using 1:10 title "E9" w l,\
     "wavefns_even.txt" using 1:11 title "E10" w l
set xlabel "x"
set ylabel "wavefunction"
set label "Plot of Even Parity Wavefunctions" at -10, 1.03
set output "wavefns_even.ps"
set terminal postscript enhanced color
replot
quit