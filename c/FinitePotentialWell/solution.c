#define MAIN_1

#include <math.h>
#include <stdio.h>
#include "./params.h"
#include "./wavefunctions.c"
#include "../RootFindingMethods/brute_force_search.c"
#include "../RootFindingMethods/bisection.c"

#define PLOT_START -30.0
#define PLOT_END 30.0
#define PLOT_STEP 0.1
#define TOLERANCE 0.003

double func_1(double e) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    return alpha * sin(alpha * WIDTH) - beta * cos(alpha * WIDTH);
}

double func_2(double e) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    return alpha * cos(alpha * WIDTH) + beta * sin(alpha * WIDTH);
}

void compute_energy_eigvals(double *brackets, double *solns, int num_sols) {
    for(int i = 0; i < num_sols; ++i) {
        solns[i] = bisection(brackets[i], brackets[i] + STEP, TOLERANCE);
    }
}

int main() {

    double brackets_1[(int)((END - START) / STEP)], brackets_2[(int)((END - START) / STEP)];
    double solns_1[20], solns_2[20];

    int num_solns_1 = brute_force_search(brackets_1, func_1);
    int num_solns_2 = brute_force_search(brackets_2, func_2);

    compute_energy_eigvals(brackets_1, solns_1, num_solns_1);
    compute_energy_eigvals(brackets_2, solns_2, num_solns_2);

    FILE *wavefns_even, *wavefns_odd;
    wavefns_even = fopen("wavefns_even.txt", "w");
    wavefns_odd = fopen("wavefns_odd.txt", "w");

    fprintf(wavefns_even, "#x     ");
    for(int i = 0; i < num_solns_1; ++i) {
       fprintf(wavefns_even, "E%d=-%.2f       ", i + 1, solns_1[i]);
    }

    fprintf(wavefns_even, "\n");

    fprintf(wavefns_odd, "#x     ");
    for(int i = 0; i < num_solns_2; ++i) {
        fprintf(wavefns_odd, "E%d=-%.2f       ", i + 1, solns_2[i]);
    }

    fprintf(wavefns_odd, "\n");

    for(double x = -30; x <= 30; x += PLOT_STEP) {
        fprintf(wavefns_even, "%.1f  ", x);
        for(int i = 0; i < num_solns_1; ++i) {
            fprintf(wavefns_even, "%e  ", even_wavefunction(solns_1[i], x));
        }
        fprintf(wavefns_even, "\n");
    }

    for(double x = -30; x <= 30; x += PLOT_STEP) {
        fprintf(wavefns_odd, "%.1f  ", x);
        for(int i = 0; i < num_solns_2; ++i) {
            fprintf(wavefns_odd, "%e  ", odd_wavefunction(solns_2[i], x));
        }
        fprintf(wavefns_odd, "\n");
    }

    fclose(wavefns_even);
    fclose(wavefns_odd);

    return 0;
}