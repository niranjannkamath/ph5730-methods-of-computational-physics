import numpy as np
import matplotlib.pyplot as plt

def explicit_method(sol, x_steps, t_steps, interval):
    for position in range(1, x_steps - 2):
        for time in range(t_steps - 1):
            sol[position, time + 1] = interval * sol[position - 1, time] + (1 - 2 * interval) * sol[position, time] + interval * sol[position + 1, time]
    
    return sol

def initial_sol(position, time):
    if time == 0: 
        return 4 * position * (1 - position)
    else:
        return 0;

start_time, end_time, start_position, end_position = 0, 1, 0, 1

position_step_size = 0.1
time_step_size = 0.5 * position_step_size**2;

t_steps, x_steps= 30, int(1 + (end_position - start_position) / position_step_size);

diffusion_coefficient = 1;

interval = diffusion_coefficient * time_step_size / position_step_size**2;

sol, position, time = np.zeros((x_steps, t_steps)), np.zeros(x_steps), np.zeros(t_steps)

for i in range(t_steps):
    time[i] = start_time + i * time_step_size
    sol[0, i] = initial_sol(start_position, time[i])
    sol[x_steps - 1, i] = initial_sol(end_position, time[i])

for i in range(x_steps):
    position[i] = start_position + i * position_step_size
    sol[i, 0] = initial_sol(position[i], 0)

position, time = np.meshgrid(position, time)

sol = explicit_method(sol, x_steps, t_steps, interval)

fig = plt.figure(figsize=(9,8),constrained_layout=True)
plt.contourf(position, time / time_step_size ,np.transpose(sol), levels=100, cmap=plt.cm.jet)
plt.colorbar()
plt.xlabel('x',fontsize=21)
plt.ylabel('t',fontsize=21)
plt.title('FTCS - Example 1',fontsize=21)
plt.savefig('./explicit_method_plot.pdf')
plt.show()