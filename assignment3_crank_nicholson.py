import numpy as np
import matplotlib.pyplot as plt

def crank_nicholson_method(sol, x_steps, t_steps, matrix1, matrix2, interval):
    for i in range(t_steps - 1):
        temp = np.matmul(matrix2, sol[1:-1, i])
        temp[0] += interval * (sol[0, i + 1] + sol[0, i])
        temp[-1] = interval * (sol[-1, i + 1] + sol[-1, i])
        t_sol = np.linalg.solve(matrix1, temp)
        for j in range(x_steps - 2):
            sol[j + 1, i + 1] = t_sol[j]

def initial_sol(position, time):
    if time == 0:
        return 2 - 1.5 * position + np.sin(np.pi * position)
    elif (position == 0):
        return 2
    else:
        return 0.5

t_start, t_end = 0, 1 
x_start, x_end = 0, 1

t_steps = 16

x_step_size, t_step_size = 0.1, 0.05

x_steps = int(1 + (x_end - x_start) / x_step_size)

diffusion_coefficient = 1.44

interval = diffusion_coefficient * t_step_size / x_step_size ** 2

sol = np.zeros((x_steps, t_steps))
position, time = np.zeros(x_steps), np.zeros(t_steps)

for i in range(x_steps):
    position[i] = x_start + i * x_step_size
    sol[i, 0] = initial_sol(position[i], 0)

for i in range(t_steps):
    time[i] = t_start + i * t_step_size
    sol[0, i] = initial_sol(x_start, time[i])
    sol[x_steps - 1, i] = initial_sol(x_end, time[i])

position, time = np.meshgrid(position, time)

M1 = np.zeros((x_steps - 2, x_steps - 2))
M2 = np.zeros((x_steps - 2, x_steps - 2))
for i in range(x_steps - 2):
    for j in range(x_steps - 2):
        if(i == j):
            M1[i, j] = 2 + 2 * interval
            M2[i, j] = 2 - 2 * interval
        if(i == j - 1 or i == j + 1):
            M1[i, j] = -interval
            M2[i, j] = interval

crank_nicholson_method(sol, x_steps, t_steps, M1, M2, interval)

fig = plt.figure(figsize = (9, 8), constrained_layout = True)
plt.contourf(position, time / t_step_size, np.transpose(sol), levels = 100, cmap = plt.cm.jet)
plt.colorbar()
plt.xlabel('x',fontsize=21)
plt.ylabel('t',fontsize=21)
plt.title('Crank Nicolson - Example 3',fontsize=21)
plt.savefig('./crank_nicholson_method_plot.pdf')
plt.show()