import numpy as np
import matplotlib.pyplot as plt

def F(a,k):
    temp = 1-4*a*np.sin(np.pi/2.*k)
    return -np.log(abs(1-4*a*np.sin(np.pi/2.*k)**2))/a

fig = plt.figure(figsize=(9,8),constrained_layout=True)
k = np.arange(0,1,0.01)
plt.plot(k,F(0.1,k),label=r'$\alpha$=0.1')
plt.plot(k,F(0.2,k),label=r'$\alpha$=0.2')
plt.plot(k,F(0.3,k),label=r'$\alpha$=0.3')
plt.plot(k,F(0.4,k),label=r'$\alpha$=0.4')
plt.plot(k,(np.pi*k)**2,label='exact')
plt.xlabel(r'$\frac{k\Delta x}{\pi}$',fontsize=21)
plt.ylabel(r'$\frac{i \omega \Delta t}{\alpha}$',fontsize=21)
plt.grid(linewidth=0.21)
plt.xlim(0,1)
plt.ylim(0,20)
plt.legend(fontsize=15)
plt.title('Dispersion Relation curves',fontsize=21)
plt.savefig('./dispersion_relation_plot.pdf')
plt.show()