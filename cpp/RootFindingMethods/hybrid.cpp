#define MAIN

#include <iostream>
#include "bisection.cpp"
#include "newton_raphson.cpp"

double hybrid(double start, double end, double tolerance) {
    double newton_start = bisection(start, end, tolerance);
    double result = newton_raphson(newton_start, tolerance/10.0);

    return result;
}

int main() {
    cout << hybrid(0.1, 4.0, 0.003) << '\n';

    return 0;
}