#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::vector;
using std::pair;
using std::make_pair;

const double START = -100.0;
const double END = 100.0;
const double STEP = 0.1;

double f(double x) {
    return (x - 10.3) * (x - 12.4) * (x - 13.7) * (x - 22.2);
}

void brute_force_search(vector<pair<double, double> > &brackets, double (*f)(double)) {
    double i = START;

    while(i <= END) {
        if((*f)(i) * (*f)(i + STEP) <= 0) {
            brackets.push_back(make_pair(i, i + STEP));
        }

        i += STEP;
    }
}

#ifndef MAIN_1
#define MAIN_1

int main() {
    vector<pair<double, double> > brackets;

    brute_force_search(brackets, f);

    for(int i = 0; i < brackets.size(); ++i) {
        cout << brackets.at(i).first << ' ' << brackets.at(i).second << '\n';
    }

    return 0;
}

#endif