#include <iostream>
#include "func.hpp"

using std::cout;

double secant(double first, double second, double tolerance) {
    double next, diff;
    int count = 0;
    do {
        diff = (func(second) - func(first)) / (second - first);
        next = second - func(second) / diff;
        first = second;
        second = next;
        count++;
    } while(diff < tolerance);

    return next;
}

int main() {
    cout << secant(3.0, 4.0, 3E-06) << '\n';

    return 0;
}