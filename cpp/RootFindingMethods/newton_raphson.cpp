#include <iostream>
#include "func.hpp"

using std::cout;

double newton_raphson(double start, double tolerance) {
    const double step_size = 0.00001;
    double diff = (func(start + step_size) - func(start - step_size)) / (2 * step_size);

    if(func(start) < tolerance) {
        return start;
    }

    double new_start = start - func(start) / diff;

    return newton_raphson(new_start, tolerance);
}

#ifndef MAIN
#define MAIN

int main() {
    cout << newton_raphson(4.0, 0.0003) << '\n';

    return 0;
}

#endif