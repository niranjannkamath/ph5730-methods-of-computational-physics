#ifndef FUNC
#define FUNC

#include <cmath>

double func(double x) {
    return x * x - 4 * x * sin(x) + 2 * sin(x) * sin(x);
}

#endif