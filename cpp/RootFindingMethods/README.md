# THIS FOLDER CONTAINS SOLUTIONS WRITTEN IN C++ OF FIRST ASSIGNMENT ON ROOT FINDING METHODS
# IT CONTAINS THE FOLLOWING FILES

## func.hpp
Contains the function I use for finding the root.

## bisection.cpp
Contains the code for finding root using Bisection Method.

## newton_raphson.cpp
Contains the code for finding root using Newton-Raphson Method.

## hybrid.cpp
Contains the code for finding root using a hybrid of Bisection and Newton-Raphson Method.

## secant.cpp
Contains the code for finding root using Secant Method.

## brute_force_search.cpp
Contains the code for finding the intervals in which the roots of a function lie.\
\
**NOTE** In this method I have used a different function from the previous methods. This function has been defined in this same file.

## Makefile
the makefile for compiling the assignment code

**To compile any of the source code files just execute** `make <filename>` **in this directory**\
Use the filename without the `.cpp` extension\
\
**Alternatively you could compile the files by hand by executing** `g++ -o <filename> <filename>.cpp`