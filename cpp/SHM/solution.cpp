#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

using std::vector;
using std::ofstream;
using std::ios;
using std::cin;
using std::cout;

const int NUM_STEPS = 30;
const double STEP_SIZE = 0.5;
const double DAMPING_CONSTANT = 0.0;
const double AMPLITUDE = 0.0;
const double DRIVING_FREQUENCY = 0.0;

void rk2(vector<double> &u1, vector<double> &u2, double step, double k, double A, double f) {
    double k1_u2 = 0;
    for(int i = 0; i < NUM_STEPS; ++i) {
        k1_u2 = A * cos(f * step * i) - u1.at(i) - k * u2.at(i);
        u2.at(i + 1) = u2.at(i) + step * (A * cos(f * step * i + 0.5 * f * step) - (u1.at(i) + 0.5 * step * u2.at(i)) - k * (u2.at(i) + 0.5 * step * k1_u2));
        u1.at(i + 1) = u1.at(i) + step * (u2.at(i) + 0.5 * step * k1_u2) ;
    }
}
void rk4(vector<double> &u1, vector<double> &u2, double step, double k, double A, double f) {
    double k1_u1 = 0, k1_u2 = 0;
    double k2_u1 = 0, k2_u2 = 0;
    double k3_u1 = 0, k3_u2 = 0;
    double k4_u1 = 0, k4_u2 = 0;
    for(int i = 0; i < NUM_STEPS; ++i) {
        k1_u1 = step * u2.at(i);
        k1_u2 = step * (A * cos(f * step * i) - u1.at(i) - k * u2.at(i));

        k2_u1 = step * (u2.at(i) + 0.5 * k1_u2);
        k2_u2 = step * (A * cos(f * step * i + 0.5 * f * step) - (u1.at(i) + 0.5 * k1_u1) - k * (u2.at(i) + 0.5 * k1_u2));

        k3_u1 = step * (u2.at(i) + 0.5 * k2_u2);
        k3_u2 = step * (A * cos(f * step * i + 0.5 * f * step) - (u1.at(i) + 0.5 * k2_u1) - k * (u2.at(i) + 0.5 * k2_u2));

        k4_u1 = step * (u2.at(i) + k3_u2);
        k4_u2 = step * (A * cos(f * step * i + f * step) - (u1.at(i) + k3_u1) - k * (u2.at(i) + k3_u2));

        u2.at(i + 1) = u2.at(i) + (1.0/6.0) * (k1_u2 + 2 * k2_u2 + 2 * k3_u2 + k4_u2);
        u1.at(i + 1) = u1.at(i) + (1.0/6.0) * (k1_u1 + 2 * k2_u1 + 2 * k3_u1 + k4_u1);
    }
}

int main() {
    vector<double> u1_rk2(NUM_STEPS + 1), u2_rk2(NUM_STEPS + 1);
    vector<double> u1_rk4(NUM_STEPS + 1), u2_rk4(NUM_STEPS + 1);

    u1_rk2.at(0) = 0.2;
    u2_rk2.at(0) = 0.0;

    u1_rk4.at(0) = 0.2;
    u2_rk4.at(0) = 0.0;

    rk2(u1_rk2, u2_rk2, STEP_SIZE, DAMPING_CONSTANT, AMPLITUDE, DRIVING_FREQUENCY);
    rk4(u1_rk4, u2_rk4, STEP_SIZE, DAMPING_CONSTANT, AMPLITUDE, DRIVING_FREQUENCY);

    ofstream output_rk2, output_rk4;
    output_rk2.open("rk2.txt", ios::trunc);
    output_rk4.open("rk4.txt", ios::trunc);
    for(int i = 0; i < NUM_STEPS + 1; ++i) {
        output_rk2 << STEP_SIZE * i << '\t' << u1_rk2.at(i) << '\n';
        output_rk4 << STEP_SIZE * i << '\t' << u1_rk4.at(i) << '\n';
    }

    output_rk2.close();
    output_rk4.close();

    return 0;
}