plot "rk2.txt" using 1:2 title "Euler's Midpoint(RK2) Method" w l, \
     "rk4.txt" using 1:2 title "RK4 Method" w l
set xlabel "time"
set ylabel "Angular Displcement"
set label "Plot of SHM" at 7, 0.23
set output "plots.ps"
set terminal postscript enhanced color
replot
quit