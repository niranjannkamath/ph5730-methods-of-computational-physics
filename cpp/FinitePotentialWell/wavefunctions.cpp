#include <cmath>
#include "./params.hpp"

double even_wavefunction(double e, double x) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    double coeff = cos(alpha * WIDTH) / exp(-beta * WIDTH);

    if(x < -WIDTH || x > WIDTH) {
        return coeff * exp(-beta * abs(x));
    }
    else {
        return cos(alpha * x);
    }
}

double odd_wavefunction(double e, double x) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    double coeff = sin(alpha * WIDTH) / exp(-beta * WIDTH);

    if(x < -WIDTH || x > WIDTH) {
        return (abs(x) / x) * (coeff * exp(-beta * abs(x)));
    }
    else {
        return sin(alpha * x);
    }
}