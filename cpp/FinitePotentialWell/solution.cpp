#define MAIN_1

#include <cmath>
#include <fstream>
#include "./params.hpp"
#include "./wavefunctions.cpp"
#include "../RootFindingMethods/brute_force_search.cpp"
#include "../RootFindingMethods/bisection.cpp"

using std::ofstream;
using std::ios;

const double TOLERANCE = 0.003;

double func_1(double e) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    return alpha * sin(alpha * WIDTH) - beta * cos(alpha * WIDTH);
}

double func_2(double e) {
    double alpha = sqrt(DEPTH - e), beta = sqrt(e);
    return alpha * cos(alpha * WIDTH) + beta * sin(alpha * WIDTH);
}

vector<double> compute_energy_eigvals(const vector<pair<double, double> > &brackets) {
    vector<double> solns;
    for(int i = 0; i < brackets.size(); ++i) {
        solns.push_back(bisection(brackets.at(i).first, brackets.at(i).second, TOLERANCE));
    }

    return solns;
}

int main() {

    vector<pair<double, double> > brackets_1, brackets_2;
    vector<double> solns_1, solns_2;

    brute_force_search(brackets_1, func_1);
    brute_force_search(brackets_2, func_2);

    solns_1 = compute_energy_eigvals(brackets_1);
    solns_2 = compute_energy_eigvals(brackets_2);

    ofstream wavefns_even, wavefns_odd;
    wavefns_even.open("wavefns_even.txt", ios::out | ios::trunc);
    wavefns_odd.open("wavefns_odd.txt", ios::out | ios::trunc);

    const double STEP = 0.1;

    wavefns_even << "#x" << "     ";
    for(int i = 0; i < solns_1.size(); ++i) {
        wavefns_even << "E" << (i + 1) << '=' << -solns_1.at(i) << "     ";
    }

    wavefns_even << '\n';

    wavefns_odd << "#x" << "     ";
    for(int i = 0; i < solns_2.size(); ++i) {
        wavefns_odd << "E" << (i + 1) << '=' << -solns_2.at(i) << "     ";
    }

    wavefns_odd << '\n';

    for(double x = -30; x <= 30; x += STEP) {
        wavefns_even << x << "  ";
        for(int i = 0; i < solns_1.size(); ++i) {
            wavefns_even << even_wavefunction(solns_1.at(i), x) << "  ";
        }
        wavefns_even << '\n';
    }

    for(double x = -30; x <= 30; x += STEP) {
        wavefns_odd << x << "  ";
        for(int i = 0; i < solns_2.size(); ++i) {
            wavefns_odd << odd_wavefunction(solns_2.at(i), x) << "  ";
        }
        wavefns_odd << '\n';
    }

    wavefns_even.close();
    wavefns_odd.close();

    return 0;
}