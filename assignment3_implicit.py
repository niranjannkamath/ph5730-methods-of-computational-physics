import numpy as np
import matplotlib.pyplot as plt

def implicit_method(sol, x_steps, t_steps, matrix, interval):
    for i in range(t_steps - 1):
        temp = np.array(sol[1:-1,i])
        t_sol = np.linalg.solve(matrix, temp)
        for j in range(x_steps - 2):
            sol[j + 1, i + 1] = t_sol[j]
        sol[0, i + 1] = sol[1, i + 1]
        sol[-1, i + 1] = sol[-2, i + 1]

def initial_sol(position, time):
    return np.exp(-position**2)

t_start, t_end = 0, 1
x_start, x_end = -5, 5

x_step_size, t_step_size = 0.1, 0.05

x_steps, t_steps = int(1 + (x_end - x_start) / x_step_size), 201

diffusion_coefficient = 1

interval = diffusion_coefficient * t_step_size / x_step_size**2

sol = np.zeros((x_steps, t_steps))
position, time = np.zeros(x_steps), np.zeros(t_steps)

for i in range(x_steps):
    position[i] = x_start + i * x_step_size
    sol[i, 0] = initial_sol(position[i], 0)

for i in range(t_steps):
    time[i] = t_start + i * t_step_size

M = np.zeros((x_steps - 2, x_steps - 2))
for i in range(x_steps - 2):
    for j in range(x_steps - 2):
        if(i == j):
            if(i == 0 or i == x_steps-3):
                M[i, j] = 1 + interval
            else:
                M[i, j] = 1 + 2 * interval
        if(i == j - 1 or i == j + 1):
            M[i, j] = -interval

implicit_method(sol, x_steps, t_steps, M, interval)

fig = plt.figure(figsize=(9, 8), constrained_layout = True)
plt.contourf(position, time / t_step_size, np.transpose(sol), levels = 100, cmap = plt.cm.jet)
plt.colorbar()
plt.xlabel('x',fontsize=21)
plt.ylabel('t',fontsize=21)
plt.title('BTCS - Example 2',fontsize=21)
plt.savefig('./implicit_method_plot.pdf')
plt.show()