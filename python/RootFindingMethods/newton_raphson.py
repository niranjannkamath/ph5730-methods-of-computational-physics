from func import func as f

def newton_raphson(start, tolerance):
    step_size = 0.00001
    diff = (f(start + step_size) - f(start - step_size)) / (2 * step_size)

    if f(start) < tolerance:
        return start

    new_start = start - f(start) / diff

    return newton_raphson(new_start, tolerance)


if __name__ == "__main__":
    print(newton_raphson(4.0, 0.0003))