def f(x):
    return (x - 10.34) * (x - 13.53) * (x - 23.77)


def brute_force_search(brackets, f):
    start, end, step = 0, 9.9, 0.1
    i = start

    while i <= end:
        if(f(i) * f(i + step) < 0):
            brackets.append((i, i + step))
        
        i += step


if __name__ == "__main__":
    brackets = []

    brute_force_search(brackets, f)

    for first, second in brackets:
        print(f"{first} {second}")