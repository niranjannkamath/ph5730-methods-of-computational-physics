from math import sin;

def func(x):
    return x * x - 4 * x * sin(x) + 2 * sin(x) * sin(x)