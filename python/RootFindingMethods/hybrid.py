from bisection import bisection
from newton_raphson import newton_raphson

def hybrid(start, end, tolerance):
    newton_start = bisection(start, end, tolerance)
    result = newton_raphson(newton_start, tolerance / 10.0)

    return result


if __name__== "__main__":
    print(hybrid(3.0, 4.0, 0.003))