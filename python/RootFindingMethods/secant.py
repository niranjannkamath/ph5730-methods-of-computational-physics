from func import func as f

def secant(first, second, tolerance):
    count, diff, next_iter = 0, 0, 0
    while diff < tolerance:
        diff = (f(second) - f(first)) / (second - first)
        next_iter = second - f(second) / diff
        first = second
        second = next_iter
        count += 1

    return next_iter


if __name__ == "__main__":
    print(secant(3.0, 4.0, 3E-06))