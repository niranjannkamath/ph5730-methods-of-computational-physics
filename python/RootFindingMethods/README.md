## THIS FOLDER CONTAINS SOLUTIONS WRITTEN IN PYTHON OF FIRST ASSIGNMENT ON ROOT FINDING METHODS
## IT CONTAINS THE FOLLOWING FILES

## func.py
Contains the function I use for finding the root.

## bisection.py
Contains the code for finding root using Bisection Method.

## newton_raphson.py
Contains the code for finding root using Newton-Raphson Method.

## hybrid.py
Contains the code for finding root using a hybrid of Bisection and Newton-Raphson Method.

## secant.py
Contains the code for finding root using Secant Method.

## brute_force_search.py
Contains the code for finding the intervals in which the roots of a function lie.\
**NOTE** In this method I have used a different function from the previous methods. This function has been defined in this same file.