from func import func as f

def bisection(start, end, tolerance):
    mid = (start + end) / 2
    
    if end - start < tolerance:
        return mid
    
    if f(start) * f(mid) < 0:
        return bisection(start, mid, tolerance)
    elif f(mid) * f(end):
        return bisection(mid, end, tolerance)
    else:
        return mid;


if __name__ == "__main__":
    print(bisection(0.1, 4.0, 0.0003))