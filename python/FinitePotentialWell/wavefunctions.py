from math import cos, exp, fabs, sin, sqrt
from params import *


def even_wavefunction(e, x):
    alpha, beta = sqrt(DEPTH - e), sqrt(e)
    coeff = cos(alpha * WIDTH) / exp(-beta * WIDTH)

    if x < -WIDTH or x > WIDTH:
        return coeff * exp(-beta * fabs(x))
    else:
        return cos(alpha * x)


def odd_wavefunction(e, x):
    alpha, beta = sqrt(DEPTH - e), sqrt(e)
    coeff = cos(alpha * WIDTH) / exp(-beta * WIDTH)

    if x < -WIDTH or x > WIDTH:
        return (fabs(x) / x) * coeff * exp(-beta * fabs(x))
    else:
        return sin(alpha * x)