import numpy as np
import matplotlib.pyplot as plt

from params import *
from wavefunctions import *
from sys import path
path.append("/home/niranjan/Computational Methods/python/RootFindingMethods")

from bisection import *
from brute_force_search import *

TOLERANCE = 0.003

def func_1(e):
    alpha, beta = sqrt(DEPTH - e), sqrt(e)
    return alpha * sin(alpha * WIDTH) - beta * cos(alpha * WIDTH)


def func_2(e):
    alpha, beta = sqrt(DEPTH - e), sqrt(e)
    return alpha * cos(alpha * WIDTH) + beta * sin(alpha * WIDTH)


def compute_energy_eigvals(brackets):
    solns = []
    for first, second in brackets:
        solns.append(bisection(first, second, TOLERANCE))
    
    return solns


if __name__ == "__main__":
    brackets_1, brackets_2 = [], []

    brute_force_search(brackets_1, func_1)
    brute_force_search(brackets_2, func_2)

    solns_1, solns_2 = compute_energy_eigvals(brackets_1), compute_energy_eigvals(brackets_2)

    START, END, STEP = -30, 30, 0.1

    data_1 = np.zeros(((int)((END - START) / STEP), len(solns_1) + 1))
    data_2 = np.zeros(((int)((END - START) / STEP), len(solns_2) + 1))

    data_1[:,0] = np.arange(START, END, STEP);
    data_2[:,0] = np.arange(START, END, STEP);

    for i in range((int)((END - START) / STEP)):
        for j in range(1, len(solns_1) + 1):
            data_1[i, j] = even_wavefunction(solns_1[j - 1], data_1[i, 0])
    
    for i in range((int)((END - START) / STEP)):
        for j in range(1, len(solns_2) + 1):
            data_2[i, j] = odd_wavefunction(solns_2[j - 1], data_2[i, 0])

    for i in range(len(solns_1)):
        plt.plot(data_1[:,0], data_1[:, i + 1])
    plt.xlabel("x")
    plt.ylabel("wavefunction")
    plt.title("Plot of Even Parity Wavefunctions")
    plt.savefig("wavefns_even.pdf")

    plt.close("all")

    for i in range(len(solns_2)):
        plt.plot(data_2[:,0], data_2[:, i + 1])
    plt.xlabel("x")
    plt.ylabel("wavefunction")
    plt.title("Plot of Odd Parity Wavefunctions")
    plt.savefig("wavefns_odd.pdf")