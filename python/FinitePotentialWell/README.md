## THIS FOLDER CONTAINS SOLUTIONS WRITTEN IN Python OF SECOND ASSIGNMENT ON FINITE POTENTIAL WELL
## IT CONTAINS THE FOLLOWING FILES

## params.py
Contains values of depth and width of the well

## wavefunctions.py
Contains definitions of even and odd parity wavefunctions

## solution.py
Main module for the assignment. Contains main function and other functions for finding energy eigenvalues

## wavefns_even.pdf
Plot of even parity eigenfunctions for differnt energy eigenvalues

## wavefns_odd.pdf
Plot of odd parity eigenfunctions for differnt energy eigenvalues